package org.utechnn.downloader

import akka.routing._
import akka.actor.{ActorRef, ActorSystem, Props}
import scala.collection.immutable
import scala.math.pow


object OsmDownloader extends App {

  def getOsmLinks(fromLayer: Int, toLayer: Int, url: String): immutable.IndexedSeq[String] = {
    for (z <- fromLayer to toLayer; x <- 0 to (pow(2, z) - 1).toInt; y <- 0 to (pow(2, z) - 1).toInt)
      yield s"$url/$z/$x/$y.png"
  }

  val _system: ActorSystem = ActorSystem.create("osm-downloader-actor-system")

  val downloader = _system.actorOf(Props[DownloaderActor].withRouter(RoundRobinPool(nrOfInstances = 50)))

  val aggregatorActor: ActorRef = _system.actorOf(DownloadedAggregatorActor.props, name="aggregatorActor")

  //  "http://tile.enaikoon.de/osm"
  for (s <- getOsmLinks(0, 10, "http://192.168.1.78")) { downloader ! File(1, 1, s) }
}
