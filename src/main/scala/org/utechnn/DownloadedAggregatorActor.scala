package org.utechnn.downloader

import akka.actor.{Actor, Props}

case class SearchResultData (taskId: Int, sourceId: Int, url: String, data: Array[Byte])

class DownloadedAggregatorActor extends Actor {
  override def receive: Receive = {
    case SearchResultData(taskId, sourceId, url, data) => { println("Downloaded: " + url) }
    case _ => unhandled()
  }
}

object DownloadedAggregatorActor {
  def props = Props(classOf[DownloadedAggregatorActor])
}
