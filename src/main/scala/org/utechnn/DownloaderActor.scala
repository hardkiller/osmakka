package org.utechnn.downloader

import akka.actor.{Actor, Props}
import scala.io.Source

case class File(taskId: Int, sourceId: Int, url: String)

class DownloaderActor extends Actor {

  val aggregatorActor = context.actorSelection("/user/aggregatorActor")

  override def receive: Receive = {
    case File(taskId, sourceId, url) => {
      val result = Source.fromURL(url, "ISO-8859-1").map(_.toByte).toArray
      aggregatorActor ! SearchResultData(taskId, sourceId, url, result)
    }
    case _ => unhandled()
  }
}

object DownloaderActor {
  def props = Props(classOf[DownloaderActor])
}